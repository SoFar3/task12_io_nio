package com.epam.model;

import java.io.*;

public class CommentsReader extends InputStream {

    private BufferedReader inputStream;

    public CommentsReader(File file) throws FileNotFoundException {
        this.inputStream = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
    }

    @Override
    public int read() throws IOException {
        return inputStream.read();
    }

    public String readComment() throws IOException {
        String line = inputStream.readLine();
        int singleComment = line.indexOf("//");
        int multiComment = line.indexOf("/*");
        if (singleComment != -1) {
            return line.substring(singleComment);
        } else if (multiComment != -1) {
            StringBuilder builder = new StringBuilder();
            if (line.contains("*/")) {
                builder.append(line, multiComment, line.indexOf("*/") + 2);
                return builder.toString();
            }
            builder.append(line.substring(multiComment)).append("\n");
            while (inputStream.ready()) {
                String comment = inputStream.readLine();
                int closeCommentIndex = comment.indexOf("*/");
                if (closeCommentIndex != -1) {
                    builder.append(comment, 0, closeCommentIndex + 2);
                    break;
                }
                builder.append(comment).append("\n");
            }
            return builder.toString();
        }
        return null;
    }

    public boolean ready() throws IOException {
        return inputStream.ready();
    }

}
