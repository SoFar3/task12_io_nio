package com.epam.model;

import com.epam.exception.NotADirectoryException;

import java.io.File;

public class DirectoryXray {

    private File dir;

    public DirectoryXray() { }

    public String getDirName() {
        return dir.getName();
    }

    public void go(String dirName) throws NotADirectoryException {
        if (dirName.equals("..")) {
            this.dir = new File(dir.getParent());
        } else {
            this.dir = new File(dirName);
            isDirectory();
        }
        printContent();
    }

    @SuppressWarnings("ConstantConditions")
    private void printContent() {
        File[] files = dir.listFiles();
        if (files == null) {
            System.out.println("Empty");
        }
        for (File file : files) {
            if (file.isFile()) {
                System.out.printf("%-24s %sKB\n", file.getName(), (file.length() / 1024));
            } else {
                System.out.println(file.getName());
            }
        }
    }

    private void isDirectory() throws NotADirectoryException {
        if ( ! dir.isDirectory()) {
            throw new NotADirectoryException();
        }
    }

}
