package com.epam.model;

import java.io.Serializable;
import java.util.Objects;

public class Droid implements Serializable {

    private String model;
    private transient String alias;

    public Droid() {
    }

    public Droid(String model, String alias) {
        this.model = model;
        this.alias = alias;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Droid droid = (Droid) o;
        return Objects.equals(model, droid.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(model);
    }

    @Override
    public String toString() {
        return "Droid{" +
                "model='" + model + '\'' +
                ", alias='" + alias + '\'' +
                '}';
    }

}
