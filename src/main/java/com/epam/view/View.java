package com.epam.view;

public interface View {

    void setMenu(Menu menu);

    void showMenu();

    String userInput(String message);

    void waitForUserOption();

    boolean doAction(String key);

}
