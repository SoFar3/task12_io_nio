package com.epam.controller;

import com.epam.view.View;
import com.epam.view.ViewImpl;

public class ControllerImpl implements Controller {

    private View view;

    public ControllerImpl() {
        view = new ViewImpl();
    }

    @Override
    public void start() {

    }

}
