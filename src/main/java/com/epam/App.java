package com.epam;

import com.epam.model.CommentsReader;
import com.epam.model.DirectoryXray;
import com.epam.model.Droid;
import com.epam.model.Ship;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class App {

    public static void main(String[] args) throws IOException {
        Ship ship = new Ship();
        ship.setId(1);
        ship.setName("Eagle");

        List<Droid> droids = new ArrayList<>();
        droids.add(new Droid("r2d2", "a"));
        droids.add(new Droid("c3P0", "b"));

        ship.setDroids(droids);

        serialize(ship, "ships.ser");
        System.out.println(deserialize("ships.ser"));

        /*CommentsReader commentsReader = new CommentsReader(new File("ArrayListCopy.java"));
        while (commentsReader.ready()) {
            String comment = commentsReader.readComment();
            if (comment != null) {
                System.out.println(comment);
            }
        }*/

        DirectoryXray directoryXray = new DirectoryXray();
        directoryXray.go("C:\\Users\\ytepl\\Dropbox\\Politech\\course_3\\task12_io_nio");
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static <T extends Serializable> void serialize(T t, String fileName) {
        File file = new File(fileName);
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            if (! file.exists()) {
                file.createNewFile();
            }
            oos.writeObject(t);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    public static <T extends Serializable> List<T> deserialize(String fileName) {
        File file = new File(fileName);
        List<T> objects = new ArrayList<>();
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            if (! file.exists()) {
                throw new IOException();
            }
            while (true) {
                try {
                    objects.add((T) ois.readObject());
                } catch (EOFException | ClassNotFoundException e) {
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return objects;
    }

}
